export function get_liv_echarts(params) {
  const colors = ['#5793f3', '#d14a61', '#675bba']
  return {
    color: colors,

    tooltip: {
      trigger: 'axis'
    },
    grid: {
      right: '20%'
    },
    toolbox: {
      feature: {
        dataView: {
          show: False,
          readOnly: False
        },
        restore: {
          show: False
        },
        saveAsImage: {
          show: True
        }
      }
    },
    legend: {
      data: ['Im', 'Po', 'Vf', 'De']
    },
    xAxis: [{
      type: 'category',
      axisTick: {
        alignWithLabel: False
      },
      data: params.str_curr
    }],
    yAxis: [
      {
        type: 'value',
        name: 'Im',
        min: 0,
        position: 'left',
        axisLine: {
          lineStyle: {
            color: colors[2]
          }
        },
        axisLabel: {
          formatter: '{value}'
        }
      },
      {
        type: 'value',
        name: 'Po',
        min: 0,
        position: 'right',
        axisLine: {
          lineStyle: {
            color: colors[0]
          }
        },
        axisLabel: {
          formatter: '{value}'
        }
      },
      {
        type: 'value',
        name: 'Vf',
        min: 0,
        position: 'right',
        offset: 40,
        axisLine: {
          lineStyle: {
            color: colors[1]
          }
        },
        axisLabel: {
          formatter: '{value}'
        }
      },
      {
        type: 'value',
        name: 'De',
        min: 0,
        position: 'right',
        offset: 80,
        axisLine: {
          lineStyle: {
            color: colors[2]
          }
        },
        axisLabel: {
          formatter: '{value}'
        }
      }
    ],
    series: [
      {
        name: 'Im',
        type: 'line',

        data: params.im
      },
      {
        name: 'Vf',
        type: 'line',
        yAxisIndex: 1,
        data: params.vf
      },
      {
        name: 'Po',
        type: 'line',
        yAxisIndex: 2,
        data: params.po
      },
      {
        name: 'De',
        type: 'line',
        yAxisIndex: 3,
        data: params.de
      }
    ]
  }
}
